export function measureExecutionTime(target: any) {
    let className = target.name;
    let methodNames = Object.getOwnPropertyNames(target.prototype);
    
    for (const methodName of methodNames) {
      let originalMethod = target.prototype[methodName];
    
      if (typeof originalMethod === 'function') {
        target.prototype[methodName] = function (...args: any[]) {
          let start = performance.now();
          let result = originalMethod.apply(this, args);
          let end = performance.now();
          let executionTime = end - start;
          console.log(`Class: ${className}, Method: ${methodName}, Execution Time: ${executionTime}ms`);
          return result;
        }
      }
    }
  }