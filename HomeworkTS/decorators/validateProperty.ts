export function validateProperty(target: any, propertyName: string) {
    let propertyValue = target[propertyName];
    
    let getter = function () {
      console.log(`Getting value of ${propertyName}: ${propertyValue}`);
      return propertyValue;
    };
    
    let setter = function (newValue: any) {
      console.log(`Setting value of ${propertyName}: ${newValue}`);
      propertyValue = newValue;
    };
    
    if (delete target[propertyName]) {
      Object.defineProperty(target, propertyName, {
        get: getter,
        set: setter,
        enumerable: true,
        configurable: true
      });
    }
  }