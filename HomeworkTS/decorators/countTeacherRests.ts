export function countTeacherRests(target: any, propertyKey: string, descriptor: PropertyDescriptor) {

    let originalMethod = descriptor.value;
    let restCountMap = new Map();
    
    descriptor.value = function (...args: any[]) {
      let teacherName = this.constructor.name;
      let count = restCountMap.get(teacherName) || 0;
      count++;
      restCountMap.set(teacherName, count);
      return originalMethod.apply(this, args);
    };
    
    return descriptor;
  }