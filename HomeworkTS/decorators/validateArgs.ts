export function validateArgs(target: any, methodName: string, descriptor: PropertyDescriptor) {
    let originalMethod = descriptor.value;
    
    descriptor.value = function (...args: any[]) {
      for (const arg of args) {
        if (typeof arg !== 'string') {
          throw new Error('Invalid argument type');
        }
      }
    
      return originalMethod.apply(this, args);
    };
    
    return descriptor;
}