import { Employee } from "../classes/Employee";
import { Student } from "../classes/Student";
import { Teacher } from "../classes/Teacher";
import { ReadonlyPerson } from "../type";

export interface IRector extends ReadonlyPerson {
    teachers: Teacher[];
    students: Student[];
    employees: Employee[];
  }