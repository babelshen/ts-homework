import { Coursework } from "../classes/Coursework";
import { Role } from "../enum";

export interface IStudent {
    name: string;
    subject: string;
    grade: number[];
    coursework: null | Coursework;
    role: Role
  }