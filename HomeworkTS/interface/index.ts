import { IPerson } from "./IPerson";
import { IRector } from "./IRector";
import { IStudent } from "./IStudent";

export {IPerson, IRector, IStudent};