import {Role} from '../enum';

export interface IPerson {
    name: string;
    role: Role;
  }