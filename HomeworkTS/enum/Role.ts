export enum Role {
    Rector = 'RECTOR',
    Teacher = 'TEACHER',
    Student = 'STUDENT',
    Employees = 'EMPLOYEE',
  }