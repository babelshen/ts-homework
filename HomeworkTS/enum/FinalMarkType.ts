export enum FinalMarkType {
    Very_Bad_Marks = 1,
    Bad_Marks = 2,
    Satisfactory_Marks = 3,
    Good_Marks = 4,
    Excellent_Marks = 5,
  }