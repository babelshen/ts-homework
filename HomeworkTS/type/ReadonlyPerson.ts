import { IPerson } from "../interface";

export type ReadonlyPerson = Readonly<IPerson>;