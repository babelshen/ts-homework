export type TaskType = {
    task: string;
    specialization: string;
    isComplete: boolean;
  };