type CouseworkType = {
    name?: string;
    subject?: string;
    handedIn?: boolean;
    mark?: number;
  }

type ModificateCourseworkTypeFirst = Required<CouseworkType>;

export type ModificateCourseworkType = Omit<ModificateCourseworkTypeFirst, 'name'>;