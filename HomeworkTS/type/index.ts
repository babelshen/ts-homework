import { ReadonlyPerson } from "./ReadonlyPerson";
import { ModificateCourseworkType } from "./ModificateCourseworkType";
import { TaskType } from "./TaskType";

export {ReadonlyPerson, ModificateCourseworkType, TaskType};