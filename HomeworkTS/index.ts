import {Rector, Employee, Teacher, Student} from './classes';

let rector = new Rector('Ren Rootson');

let teacher1 = new Teacher('Bob Levison', 'Math');
let teacher2 = new Teacher('Ren Seventeen', 'Informatic');

let employee1 = new Employee('Lia Mitchel', 'cleaning');
let employee2 = new Employee('Red Sean', 'repair');

let student1 = new Student('Anna Berbock', 'Math', [5, 4, 5]);
let student2 = new Student('Greek Lemontov', 'Informatic', [4, 4, 3]);


rector.addStudent(student1);
rector.addStudent(student2);

rector.listOfStudents();

rector.hire(teacher1);
rector.hire(teacher2);
rector.hire(employee1);
rector.hire(employee2);

// rector.listOfEmployees();

rector.fire(employee1);

// rector.listOfEmployees();
// rector.listOfTeachers();

// rector.giveInstruction(employee2, 'Cleaning the area');
// rector.giveInstruction(employee2, 'Repair the desks');

// rector.listOfEmployees();
// employee2.checkTask();
// rector.listOfEmployees();

employee2.completeTask();
employee2.someRest('drink coffee');
employee2.completeTask();
rector.listOfEmployees();

/*
teacher1.addGrade(student1, 3);
teacher1.someRest('smoke');
teacher1.someRest('play');
teacher1.addGrade(student1, 3);
teacher1.addGrade(student2, 3);
rector.listOfStudents();
*/


student1.courseworkForInspection(teacher1);
student1.writeCoursework();
student1.writeCoursework();
student1.chillOut();
student1.writeCoursework();
student1.chillOut();
student1.writeCoursework();
student1.chillOut();
student1.writeCoursework();
student1.courseworkForInspection(teacher2);
student1.courseworkForInspection(teacher1);
teacher2.checkCourseworks();
teacher2.someRest('smoke');
teacher2.someRest('play');
teacher2.checkCourseworks();
rector.listOfStudents();
student1.courseworkForInspection(teacher1);
teacher1.someRest('smoke');
teacher1.someRest('play');
teacher1.checkCourseworks();
rector.listOfStudents();


// rector.resultSemester();
rector.callTeacherSomeRest(teacher1);
//rector.callTeacherSomeRest(employee2);