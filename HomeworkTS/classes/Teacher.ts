import { Role } from "../enum";
import { IPerson } from "../interface";
import { GeneralEmployee } from "./AbstractGeneralEmployee";
import { Student } from "./Student";

export class Teacher extends GeneralEmployee implements IPerson {

    checkingCoursework: Student[];
    returnCoursework: Student[];
    role: Role;
      
    constructor(name: string, public teaching: string) {
      super(name);
      this.teaching = teaching;
      this.checkingCoursework = [];
      this.returnCoursework = [];
      this.role = Role.Teacher;
    }
    
    setMood(param: number, direction: boolean): void {
      if (direction) {
        this._mood += param;
      } else {
        this._mood -= param;
      }
    }
    
    addGrade(student: Student, grade: number): void {
      if (this.getMood() <= 0) {
        console.log(`${this.name} tired - need some rest`);
      } else {
        if (this.teaching !== student.subject) {
          console.log('This student is not studying your discipline!');
          this.setMood(5, false);
        } else {
          student.setGrade(grade);
          this.setMood(10, false);
        }
      }
    }
    
    setCheckingCoursework(student: Student) {
      this.checkingCoursework.push(student);
    }
    
    checkCourseworks(): void {
      if (this.checkingCoursework.length === 0) {
        console.log('No coursework to check');
        this.setMood(10, true)
      } else { 
        if (this.getMood() <= 0) {
          console.log(`${this.name} tired - get some rest`);
        } else {
          this.returnCoursework = this.checkingCoursework.filter(
            (item) => item.coursework!.subject !== this.teaching
          );
          let checkCoursework = this.checkingCoursework.filter(
            (item) => item.coursework!.subject === this.teaching
          );
    
          if (this.returnCoursework.length !== 0) {
            console.log('The following students handed in their work to the wrong teacher:');
            this.returnCoursework.forEach((item) =>
              console.log(`${item.name} - ${item.coursework!.subject}`)
            );
            this.returnCoursework.forEach((item) => (item.coursework!.handedIn = false));
            console.log(`${this.name} returned the coursework without a mark to:`);
            this.returnCoursework.forEach((item) => console.log(`${item.name}`));
          }
  
          this.setMood(10, false)
    
          if (checkCoursework.length !== 0) {
            checkCoursework.forEach((item) => {
              let mark = Math.floor(1 + Math.random() * 5);
              item.coursework!.setMark(mark);
              this.setMood(30, false)
              console.log(`${this.name} rated ${item.name}'s work ${mark} points`);
            });
          }
        }
      }
    }
  }