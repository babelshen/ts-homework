import { TaskType } from "../type";

export class Task implements TaskType {
    task: string;
    specialization: string;
    isComplete: boolean = false;
  
    constructor(task: string, specialization: string) {
      this.task = task;
      this.specialization = specialization;
    }
  
    setComplete(): void {
      this.isComplete = true;
    }
  }
