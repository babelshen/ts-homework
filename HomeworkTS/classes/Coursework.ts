import { ModificateCourseworkType } from "../type";

export class Coursework implements ModificateCourseworkType {
    private _progress: number = 0;
  
    handedIn: boolean;
    mark: number;
  
    constructor(public subject: string) {
      this.subject = subject;
      this.handedIn = false;
      this.mark = 0;
    }
  
    getProgress(): number {
      return this._progress;
    }
    
    setProgress(): void {
      if (this._progress < 3) {
        this._progress++;
      }
    }
    
    isComplete(): boolean {
      return this._progress === 3;
    }
    
    setHandedIn(value: boolean): void {
      this.handedIn = value;
    }
    
    isHandedIn(): boolean {
      return this.handedIn;
    }
    
    setMark(mark: number): void {
      this.mark = mark;
    }
    
    hasMark(): boolean {
      return this.mark !== 0;
    }
    
    getMark(): number {
      return this.mark;
    }
  }