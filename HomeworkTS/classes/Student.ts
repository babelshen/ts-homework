import { validateArgs, validateProperty } from "../decorators";
import { Role } from "../enum";
import { IStudent } from "../interface";
import { Coursework } from "./Coursework";
import { Teacher } from "./Teacher";

export class Student implements IStudent {
    @validateProperty
    private _fatigue: boolean = false;
      
    constructor(
      public name: string, 
      public subject: string, 
      public grade: number[], 
      public coursework: null | Coursework = null, 
      public role: Role = Role.Student
    ) {}
    
    chillOut(): void {
      let averageGrade = this.grade.reduce((a, b) => a + b) / this.grade.length;
      if (this.coursework && this.coursework.mark) {
        averageGrade = (averageGrade + this.coursework.mark) / 2;
      }
      if (averageGrade < 4) {
        console.log(`${this.name} must not relax!`);
      } else {
        console.log(`Good job, ${this.name}! You can go to a party.`);
        if (this.getFatigue()) {
          this.setFatigue();
        } 
      }
    }
      
    @validateArgs
    study(subject: string): void {
      if (this.subject !== subject) {
        console.log(`${this.name} should be studying another subject - ${this.subject}!`);
      } else {
        console.log(`New assessments will help ${this.name} get extra points`);
        if (this.getFatigue()) {
          this.setFatigue();
        }
      }
    }
    
    writeCoursework(): void {
      if (this.getFatigue()) {
        console.log(`${this.name} is tired - do something else`);
      } else if (!this.coursework) {
        this.coursework = new Coursework(this.subject);
        console.log(`${this.name}'s coursework has started`);
        this.setFatigue()
        this.coursework.setProgress();
      } else if (!this.coursework.isComplete()) {
        this.coursework.setProgress();
        this.setFatigue()
        console.log(`${this.name}'s coursework is ${this.coursework.getProgress()}/3 complete`);
      } else {
        console.log(`${this.name}'s coursework is done. Hand it in for assessment.`);
      }
    }
    
      courseworkForInspection(teacher: Teacher): void {
        if (this.coursework && this.coursework.isComplete()) {
          if (!this.coursework.isHandedIn()) {
            teacher.setCheckingCoursework(this);
            this.coursework.setHandedIn(true);
            console.log(`${this.name} handed in coursework to teacher ${teacher.name}`);
          } else {
            console.log(`${this.name} has already handed in coursework!`);
          }
        } else {
          console.log(`${this.name}'s coursework is not ready yet`);
        }
      }
    
      getMarkForCourseWork(): void {
        if (!this.coursework || !this.coursework.isHandedIn()) {
          console.log(`${this.name} has not yet handed in coursework`);
        } else if (!this.coursework.hasMark()) {
          console.log(`${this.name}'s coursework has not yet been evaluated`);
        } else {
          console.log(`${this.name}'s mark for the coursework is ${this.coursework.getMark()}`);
        }
      }
    
      setGrade(grade: number): void {
        this.grade.push(grade);
      }
    
      getFatigue(): boolean {
        return this._fatigue;
      }
    
      setFatigue(): void {
        this._fatigue = !this._fatigue;
      }
  
      tryEscape(): never {
        throw new Error(`My name is ${this.name}. I'm from the 2050 year and I'm a bum. Is that the fate you want?`)
      }
    }