import { Rector } from "./Rector";
import { GeneralEmployee } from './AbstractGeneralEmployee';
import { Employee } from "./Employee";
import { Teacher } from "./Teacher";
import { Student } from "./Student";
import { Coursework } from "./Coursework";
import { Task } from "./Task";

export {Rector, GeneralEmployee, Employee, Teacher, Student, Coursework, Task};