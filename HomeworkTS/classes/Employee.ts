import { Role } from "../enum";
import { IPerson } from "../interface";
import { GeneralEmployee } from "./AbstractGeneralEmployee";
import { Task } from "./Task";

export class Employee extends GeneralEmployee implements IPerson {
    role: Role;
    tasks: Task[];
  
    constructor(name: string, public specialization: string) {
      super(name);
      this.specialization = specialization;
      this.role = Role.Employees;
      this.tasks = [];
    }
    
    setMood(param: number, direction: boolean): void {
      if (direction) {
        this._mood += param;
      } else {
        this._mood -= param;
      }
    }
  
    getTasks(): Task[] {
      return this.tasks;
    }
  
    setTasks(task: Task): void {
      this.tasks.push(task);
    }
  
    checkTask() {
      this.tasks.forEach((item, index) => {
        if (item.specialization !== this.specialization) {
          console.log(`${this.name} reported that the assignment ${item.task} was not up to his competence`);
          this.tasks.splice(index, 1);
          this.setMood(5, true);
        } else {
          console.log(item);
        }
      });
    }
  
    completeTask() {
      if (this.getMood() <= 0) {
        console.log(`${this.name} is out of tune. Do something nice`);
      } else if (this.tasks.length === 0) {
        console.log(`${this.name} has not tasks for today`);
        this.setMood(5, true);
      } else {
        this.tasks.forEach(item => {
          item.setComplete();
          this.setMood(10, false);
        });
        console.log(`${this.name} did a good job and completed all the tasks`);
      }
    }
  }