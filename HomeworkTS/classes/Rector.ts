import { measureExecutionTime } from "../decorators";
import { IRector } from "../interface";
import { Role, FinalMarkType } from "../enum";
import { Teacher } from "./Teacher";
import { Student } from "./Student";
import { Employee } from "./Employee";
import { Task } from "./Task";

@measureExecutionTime
export class Rector implements IRector {
  teachers: Teacher[];
  students: Student[];
  employees: Employee[];
  role: Role;

  constructor(public name: string) {
    this.teachers = [];
    this.students = [];
    this.employees = [];
    this.role = Role.Rector
  }

  addStudent(student: Student): void {
    this.students.push(student);
    console.log(`Student ${student.name} is admitted to university`);
  }
  
  removeStudent(student: Student): void {
    this.students = this.students.filter((item) => item.name !== student.name);
    console.log(`Student ${student.name} expelled from university`);
  }

  hire(person: Teacher | Employee): void | never {
    if (person instanceof Teacher) {
      this.teachers.push(person);
      console.log(`You hired ${person.name} for a job`);
    } else if (person instanceof Employee) {
      this.employees.push(person);
      console.log(`You hired ${person.name} for a job`);
    } else {
      throw new Error ('You need to specify a teacher or an employee');
    }
  }

  isTeacher(person: Teacher | Employee): person is Teacher {
    return person.role === Role.Teacher;
  }
  
  isEmployee(person: Teacher | Employee): person is Employee {
    return person.role === Role.Employees;
  }
  
  fire(person: Teacher | Employee): void | never {
    if (this.isTeacher(person)) {
      this.teachers = this.teachers.filter((item) => item.name !== person.name);
      console.log(`You fired ${person.name} from his job`);
    } else if (this.isEmployee(person)) {
      this.employees = this.employees.filter((item) => item.name !== person.name);
      console.log(`You fired ${person.name} from his job`);
    } else {
      throw new Error('You need to specify a teacher or an employee');
    }
  }
  
  listOfStudents(): void {
    if (this.students.length !== 0) {
      this.students.forEach((item) => {
        console.log(item);
      });
    } else {
      console.log(`We don't have students at university!`);
    }
  }
  
  listOfTeachers(): void {
    if (this.teachers.length !== 0) {
      this.teachers.forEach((item) => {
        console.log(item);
      });
    } else {
      console.log(`We don't have teachers at university!`);
    }
  }

  listOfEmployees(): void {
    if (this.employees.length !== 0) {
      this.employees.forEach((item) => {
        console.log(item);
      });
    } else {
      console.log(`We don't have employees at university!`);
    }
  }

  giveInstruction(person: Employee, action: string): void {
    if (this.employees.find(item => item.name === person.name)) {
      let task: string;
      let specialization: string;
      let createTask: Task;
      switch (action) {
        case 'Cleaning the area':
          task = 'Cleaning the area';
          specialization = 'cleaning';
          createTask = new Task (task, specialization);
          person.setTasks(createTask);
          console.log(`You've got ${task} ${person.name}`);
          break;
        case 'Cleaning the premises':
          task = 'Cleaning the premises';
          specialization = 'cleaning';
          createTask = new Task (task, specialization);
          person.setTasks(createTask);
          console.log(`You've got ${task} ${person.name}`);
          break;
        case 'Repair the desks':
          task = 'Repair the desks';
          specialization = 'repair';
          createTask = new Task (task, specialization);
          person.setTasks(createTask);
          console.log(`You've got ${task} ${person.name}`);
          break;
        case 'Repair microphones':
          task = 'Repair microphones';
          specialization = 'repair';
          createTask = new Task (task, specialization);
          person.setTasks(createTask);
          console.log(`You've got ${task} ${person.name}`);
          break;
        default:
          console.log(`${action} - unknown action`);
          break;
      }
    } else {
      throw new Error ('There is no such employee on the staff')
    }
  }

  resultSemester(): void {
    if (this.students.length <= 0) {
      console.log('It is not possible to generate information.');
    } else {
      let totalStudents = this.students.length;
      let totalStudentsWithoutCoursework = 0;
      let totalStudentsWithoutMarkForCoursework = 0;
      let arrayOfMarks: number[] = [];
      
      this.students.forEach(item => {
        if (item.coursework === null) {
          totalStudentsWithoutCoursework += 1;
        } else if (item.coursework !== null && item.coursework.getMark() === 0) {
          totalStudentsWithoutMarkForCoursework += 1;
        }
          arrayOfMarks.push(...item.grade);
        });
      
        let averageMark: number = Math.round(arrayOfMarks.reduce((a, b) => a + b) / arrayOfMarks.length);
        let message = '';

        switch (averageMark) {
          case FinalMarkType.Very_Bad_Marks:
            message = "Very Bad Marks";
            break;
          case FinalMarkType.Bad_Marks:
            message = "Bad Marks";
            break;
          case FinalMarkType.Satisfactory_Marks:
            message = "Satisfactory Marks";
            break;
          case FinalMarkType.Good_Marks:
            message = "Good Marks";
            break;
          case FinalMarkType.Excellent_Marks:
            message = "Exellent Marks";
            break;
          default:
            message = "Unknown";
            break;
        }
  
        console.log(`The number of students at the university: ${totalStudents}
    The number of students without coursework: ${totalStudentsWithoutCoursework}
    The number of students without mark for coursework: ${totalStudentsWithoutMarkForCoursework}
    Assessment of student performance: ${message}`);
    }
  }

  callTeacherSomeRest(person: Teacher): void | never {
    if (person instanceof Teacher) {
      let teacherName = person.constructor.name;
      let count = person.getRestCount();
      console.log(`${teacherName} has rested ${count} times.`);
    } else {
      throw new Error ('This function can only be applied to teachers');
    }
  }
}