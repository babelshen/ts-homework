import { countTeacherRests, validateArgs, validateProperty } from "../decorators";

export abstract class GeneralEmployee {
    @validateProperty
    protected _mood: number = 0;
    private _restCount: number = 0;
    
    constructor(public name: string) {}
    
    getMood(): number {
      return this._mood;
    }
  
    @countTeacherRests
    @validateArgs
    someRest(action: string) {
      this._restCount++;
  
      switch (action) {
        case 'smoke':
          this.setMood(5, true);
          console.log(`${this.name} smoked`);
          break;
        case 'drink coffee':
          this.setMood(10, true);
          console.log(`${this.name} took a cup of coffee`)
          break;
        case 'play':
          this.setMood(20, true);
          console.log(`${this.name} plaied`)
          break;
        case 'discuss the students':
          this.setMood(50, true);
          console.log(`${this.name} made fun of the students in the teachers' lounge`)
          break;
        default:
          console.log(`${this.name} had a good time, but he feels empty`);
          break;
      }
    }
  
    getRestCount(): number {
      return this._restCount;
    }
    
    abstract setMood(param: number, direction: boolean): void;
}